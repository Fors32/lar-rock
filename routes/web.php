<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
});
//Route::get('/categories', ['as' => 'categories', 'uses' => 'Site\HomeController@categories']);


Route::group(['prefix' => 'categories'], function () {
	Route::get('/', ['as' => 'categories', 'uses' => 'Site\CategoryController@index']);
	Route::get('/{url}', ['as' => 'category', 'uses' => 'Site\CategoryController@getProductsByCategory']);
	Route::post('product-ajax', ['as' => 'product-ajax', 'uses' => 'Site\ProductController@getProductAjax']);
});
