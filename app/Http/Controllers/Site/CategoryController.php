<?php

namespace App\Http\Controllers\Site;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Site
 */
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
//    	dump(Category::all());

	    $products = Product::all();
        return view('site.categories', [
            'categories' => Category::all(),
	        'products' => $products
        ]);
    }

    public function getProductsByCategory($url)
    {
//    	dump(Category::all());
	    $active_category = Category::where(['url' => $url])->first();

	    $products = Product::all();

//	    dump($products);
        return view('site.categories', [
            'categories' => Category::all(),
	        'products' => $products,
	        'active' => $active_category,
	        'url' => $url
        ]);
    }

    public function view($slug)
    {
        $city = Cities::where(['slug' => $slug])->first();
        $districts = District::where(['city_id' => $city->id])->get();

        return view('site.districts', [
            'districts' => $districts,
            'city' => $city
        ]);
    }
}