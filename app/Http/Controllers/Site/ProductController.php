<?php

namespace App\Http\Controllers\Site;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Site
 */
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function getProductAjax(Request $request){
//		dump($request->get('id'));
	    $id = $request->get('id');
	    $product = Product::where(['id' => $id])->first();
	    return view('site.includes.product-modal', [
	    	'product' => $product
	    ]);
    }
}