<?php

namespace App;

use Illuminate\Support\Collection;
use Larrock\ComponentCatalog\Models\Catalog;
use Larrock\ComponentCategory\Facades\LarrockCategory;
use Larrock\ComponentPages\Facades\LarrockPages;
use Larrock\Core\Helpers\MessageLarrock;
use Larrock\Core\Traits\GetFilesAndImages;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Product extends Catalog
{

	use GetFilesAndImages;
//	use HasMediaTrait;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	public $config = [
		'model' => 'Larrock\ComponentCatalog\Models\Catalog'
	];

//	public function getImages()
//	{
//		return $this->hasMany('Spatie\MediaLibrary\Media', 'model_id', 'id')->where([['model_type', '=', Catalog::getModelName()], ['collection_name', '=', 'images']])->orderBy('order_column', 'DESC');
//	}

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
	}

	public function getFirstImage()
	{
		return parent::getFirstImage();
	}

//	public function getMedia(string $collectionName = 'images', $filters = []): Collection
//	{
//		return Catalog::getMedia($collectionName, $filters);
//	}

//	public function getImages()
//	{
//		return $this->hasMany('Spatie\MediaLibrary\Media', 'model_id', 'id')->where([['model_type', '=', LarrockPages::getModelName()], ['collection_name', '=', 'images']])->orderBy('order_column', 'DESC');
//	}

	public function getCategoryUrl()
	{

		if ($this->getLink(LarrockCategory::getModelName())->first()) {
			foreach ($this->getLink(LarrockCategory::getModelName())->first()->parent_tree as $category) {
				$url = $category->url;
			}

			return $url;
		}
		MessageLarrock::danger('Раздел из связи более не существует. Товар: '.$this->id.' '.$this->title.'. Ссылка на товар не будет сгенерирована', true);

		return null;
	}
}
