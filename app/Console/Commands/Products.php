<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;
use Larrock\ComponentCatalog\Models\Catalog;
use Larrock\Core\Models\Link;

class Products extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Product::all() as $product){
        	dump($product->category_id);
        	$new_product = new Catalog;
        	$new_product->title = $product->name;
        	$new_product->short = $product->name;
        	$new_product->description = $product->description;
        	$new_product->url = $product->slug;
        	$new_product->description_link = 'Feed';
        	$new_product->what = 'грн';
        	$new_product->cost_old = $product->old_price;
        	$new_product->cost = $product->price;
        	$new_product->manufacture = 'parse';
        	$new_product->position = 0;
        	$new_product->active = 1;
        	$new_product->nalichie = 9999;
        	$new_product->sales = 0;

        	$new_product->save();

        	$new_link = new Link;

        	$new_link->id_parent = $new_product->id;
        	$new_link->id_child = $product->category_id;
        	$new_link->model_parent = 'Larrock\ComponentCatalog\Models\Catalog';
        	$new_link->model_child = 'Larrock\ComponentCategory\Models\Category';

        	$new_link->save();
        }
    }
}
